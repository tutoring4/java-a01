import java.util.ArrayList;
import java.util.List;

public class A01Q02 {
    public static List<Integer> findSmallestFactors(int num) {
        int divider = 2;
        int target = num;
        List<Integer> factors = new ArrayList<>();
        while(target != 0) {
            int isDividable = target%divider;
            if(isDividable == 0) {
                factors.add(divider);
                target = target/divider;
            } else {
                divider++;
            }
            if(target == 1) {
                break;
            }
        }
        return factors;
    }
}
