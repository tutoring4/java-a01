public class A01Q01 {
    public static boolean checkPalindrome(Integer num) {
        String str = num.toString();

        while(str.length() >= 2) {
            String first = str.substring(0, 1);
            String last = str.substring(str.length()-1);
            if(!first.equals(last)) {
                return false;
            }
            str = str.substring(1, str.length()-1);
        }
        return true;
    }
}