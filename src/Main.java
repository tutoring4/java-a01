import java.util.List;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        boolean a1 = A01Q01.checkPalindrome(121);
        System.out.println(a1);
        List<Integer> a2 = A01Q02.findSmallestFactors(120);
        System.out.println(a2);
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a number 1: ");
        double n1 = reader.nextInt();
        System.out.println("Enter a number 2: ");
        double n2 = reader.nextInt();
        System.out.println("Enter a number 3: ");
        double n3 = reader.nextInt();
        A01Q03.displaySortedNumbers(n1,n2,n3);
        reader.close();
    }
}