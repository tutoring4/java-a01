import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class A01Q03 {
    public static void displaySortedNumbers(double num1, double num2, double num3) {
        List<Double> sorted = sort(num1, num2, num3);
        sorted.forEach(System.out::println);
    }

    private static List<Double> sort(Double ...args) {
        List<Double> arr = Arrays.asList(args);
        return arr.stream()
                .sorted()
                .collect(Collectors.toList());
    }

}
