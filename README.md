A01Q01
1. the function takes an integer and convert it into a string first
2. loop until the string of number does not have more than 2 characters
3. get the first and last character from the string and compare
4. if they are the same, keep going on until the condition break. otherwise break it. which means it is not palindrome

A01Q02
1. create an int variable starting from 2.
2. keep dividing by the variable
3. if you cannot divide, increment the variable by 1
4. if the number is equal to 1, then the collection of divider is the smallest factors

A01Q03
1. the main function takes 3 numbers 
2. convert those numbers as a Collection object
3. by doing so, java8 stream can be used
4. java8 stream has a method called sorted and apply to the collection object 